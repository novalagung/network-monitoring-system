package console

import (
	"monitormap/webapp/app/models"
	"monitormap/webapp/app/modules"
	"os"

	db "github.com/eaciit/dbox"
	tk "github.com/eaciit/toolkit"
	"gopkg.in/mgo.v2/bson"
)

func ClearData(tableName string) {
	modules.Conn.NewQuery().
		From(tableName).
		Delete().
		Exec(nil)
}

func InjectDataHost() {
	description := `Lorem ipsum dolor sit amet, consectetur adipiscing elit. In est leo, ultrices eget dignissim eget, commodo vitae nunc. Praesent a mi quis nunc suscipit ullamcorper vel sed libero. Nam arcu leo, porttitor et lorem ac, molestie imperdiet ligula. Donec a turpis lobortis neque molestie feugiat id et mauris. Fusce tincidunt orci non velit feugiat volutpat`

	data := []models.HostModel{
		models.HostModel{IP: "216.58.212.174", Name: "Server Lemhbang", Latitude: "-7.703433", Longitude: "112.714920", Description: description, SID: "SERV_LEMHBANG", Bandwidth: "100 Mbps", MRTG_IMG_URL: "/static/core/images/map-detail-placeholder.png"},
		models.HostModel{IP: "112.3.1.12", Name: "Server Karangsono", Latitude: "-7.731001", Longitude: "112.729969", Description: description, SID: "SERV_KARANGSONO", Bandwidth: "100 Mbps", MRTG_IMG_URL: "/static/core/images/map-detail-placeholder.png"},
		models.HostModel{IP: "98.138.252.38", Name: "Server Randu Gong", Latitude: "-7.723516", Longitude: "112.865925", Description: description, SID: "SERV_RANDU_GONG", Bandwidth: "100 Mbps", MRTG_IMG_URL: "/static/core/images/map-detail-placeholder.png"},
		models.HostModel{IP: "98.138.252.38", Name: "Server Baledono", Latitude: "-7.845293", Longitude: "112.890644", Description: description, SID: "SERV_BALEDENO", Bandwidth: "100 Mbps", MRTG_IMG_URL: "/static/core/images/map-detail-placeholder.png"},
		models.HostModel{IP: "1.23.123.44", Name: "Server Tutur", Latitude: "-7.861618", Longitude: "112.808247", Description: description, SID: "SERV_TUTUR", Bandwidth: "100 Mbps", MRTG_IMG_URL: "/static/core/images/map-detail-placeholder.png"},
	}

	query := modules.Conn.NewQuery().
		From(models.NewHostModel().TableName()).
		SetConfig("multiexec", true).
		Save()

	for _, each := range data {
		each.Id = bson.NewObjectId().Hex()
		err := query.Exec(tk.M{"data": each})
		if err != nil {
			tk.Println("Error InjectDataHost", err.Error())
		}
	}

	if query != nil {
		query.Close()
	}
}

func InjectDefaultUser() {
	data := []models.UserModel{
		models.UserModel{Email: "admin@nms.com", Password: "admin", Name: "User " + models.OptionRoleAdmin.Name, IsNew: true, Role: models.OptionRoleAdmin.Value},
		models.UserModel{Email: "superadmin@nms.com", Password: "superadmin", Name: "User " + models.OptionRoleSuperAdmin.Value, IsNew: true, Role: models.OptionRoleSuperAdmin.Value},
		models.UserModel{Email: "readonly@nms.com", Password: "readonly", Name: "User " + models.OptionRoleReadOnly.Name, IsNew: true, Role: models.OptionRoleReadOnly.Value},
	}

	query := modules.Conn.NewQuery().
		From(models.NewUserModel().TableName()).
		SetConfig("multiexec", true).
		Save()

	for _, each := range data {
		each.Id = bson.NewObjectId().Hex()
		each.Password = each.HashPassword()
		err := query.Exec(tk.M{"data": each})
		if err != nil {
			tk.Println("Error InjectDefaultUser", err.Error())
		}
	}

	if query != nil {
		query.Close()
	}
}

func GeneratePredefinedDataDevelopment() {
	ClearData(models.NewPingModel().TableName())
	ClearData(models.NewPingHistoryModel().TableName())
	ClearData(models.NewUserModel().TableName())
	ClearData(models.NewHostModel().TableName())

	InjectDataHost()
	InjectDefaultUser()
}

func GeneratePredefinedDataProduction() {
	// create superadmin user IF NOT EXISTS
	// nothing deleted

	csr, err := modules.Conn.NewQuery().
		From(models.NewUserModel().TableName()).
		Where(db.Eq("Role", models.OptionRoleSuperAdmin.Value)).
		Cursor(nil)
	if err != nil {
		tk.Println("Error GeneratePredefinedDataProduction", err.Error())
		os.Exit(0)
	}

	result := make([]models.UserModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		tk.Println("Error GeneratePredefinedDataProduction", err.Error())
		os.Exit(0)
	}

	if len(result) == 0 {
		userData := models.UserModel{Email: "superadmin@nms.com", Password: "superadmin", Name: "User " + models.OptionRoleSuperAdmin.Name, IsNew: true, Role: models.OptionRoleSuperAdmin.Value}

		err := modules.Conn.NewQuery().
			From(models.NewUserModel().TableName()).
			Save().
			Exec(tk.M{"data": userData})
		if err != nil {
			tk.Println("Error GeneratePredefinedDataProduction", err.Error())
			os.Exit(0)
		}
	}
}
