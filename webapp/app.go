package main

import (
	"log"
	"monitormap/consoleapp"
	"monitormap/webapp/app/controllers"
	"monitormap/webapp/app/models"
	"monitormap/webapp/app/modules"
	"net/http"
	"os"
	"path/filepath"

	tk "github.com/eaciit/toolkit"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func main() {
	modules.InitConfig()
	modules.InitDatabase()
	models.InitTelegramBot()
	models.InitPinger()

	if modules.Config.GetString("environment") == "production" {
		controllers.ReloadViews()
		gin.SetMode(gin.ReleaseMode)
		console.GeneratePredefinedDataProduction()
	} else {
		console.GeneratePredefinedDataDevelopment()
	}

	modules.Router.Use(sessions.Sessions("mysession", sessions.NewCookieStore([]byte(modules.SessionKey))))

	// ====== routes

	modules.Router.Static("/static", filepath.Join(modules.BasePath, "app/assets"))
	modules.Router.GET("/", func(c *gin.Context) {
		sess := sessions.Default(c)
		sessionEmail := ""
		if vl := sess.Get(models.SessionKeyEmail); vl != nil {
			sessionEmail = vl.(string)
		}

		if sessionEmail == "" {
			c.Redirect(http.StatusTemporaryRedirect, "/page/login")
		} else {
			c.Redirect(http.StatusTemporaryRedirect, "/page/map")
		}
	})

	authController := controllers.AuthController{}
	modules.Router.GET("/page/login", authController.PageIndex())
	modules.Router.POST("/login", authController.DoLogin)
	modules.Router.GET("/logout", authController.DoLogout)

	mapController := controllers.MapController{}
	modules.Router.GET("/page/map", mapController.PageMap())
	modules.Router.GET("/page/map/:id", mapController.PageMapDetail())

	hostController := controllers.HostController{}
	modules.Router.GET("/page/master/host", hostController.PageIndex())
	modules.Router.GET("/master/host/detail/:id", hostController.ReadDetail)
	modules.Router.GET("/master/host/without-histories", hostController.ReadWithoutHistories)
	modules.Router.POST("/master/host", hostController.Save)
	modules.Router.DELETE("/master/host", hostController.Delete)

	userController := controllers.UserController{}
	modules.Router.GET("/page/master/user", userController.PageIndex())
	modules.Router.GET("/master/user", userController.Read)
	modules.Router.POST("/master/user", userController.Save)
	modules.Router.DELETE("/master/user", userController.Delete)

	// ====== start app
	RunApp()
}

func RunApp() {
	port := ":" + tk.ToString(modules.Config.GetInt("port"))
	log.Println("Starting application at port", port)

	err := modules.Router.Run(port)
	if err != nil {
		log.Println(err.Error())
		os.Exit(0)
	}
}
