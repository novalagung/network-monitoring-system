package models

import (
	"bytes"
	"fmt"
	"monitormap/webapp/app/modules"

	"github.com/joho/godotenv"
	"github.com/rockneurotiko/go-tgbot"
)

var availableCommands = map[string]string{
	"/start": "Start receiving status",
	"/help":  "Get help",
	"/stop":  "Stop receiving status",
}

var (
	bot         *tgbot.TgBot
	channel     chan string
	shouldStart bool
	token       string
)

func InitTelegramBot() {
	fmt.Println("=====> Start Telegram bot")

	godotenv.Load("secrets.env")

	token = modules.Config.GetString("telegramBotToken")
	channel = make(chan string)

	bot = tgbot.NewTgBot(token).
		SimpleCommandFn(`stop`, func(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
			fmt.Println("request /stop")

			if shouldStart {
				bot.SimpleSendMessage(msg, "Stop receiving status")
				shouldStart = false
			}

			return nil
		}).
		SimpleCommandFn(`start`, func(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
			fmt.Println("request /start")

			bot.SimpleSendMessage(msg, "Start receiving status")
			shouldStart = true

			for shouldStart {
				select {
				case message := <-channel:
					if shouldStart {
						fmt.Println("receiving", message)
						bot.SimpleSendMessage(msg, message)
					}
				}
			}
			return nil
		}).
		SimpleCommandFn(`help`, func(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
			fmt.Println("request /help")

			var buffer bytes.Buffer
			for cmd, htext := range availableCommands {
				str := fmt.Sprintf("%s - %s\n", cmd, htext)
				buffer.WriteString(str)
			}

			res := buffer.String()
			return &res
		})

	bot.DefaultDisableWebpagePreview(true)
	bot.DefaultOneTimeKeyboard(true)
	bot.DefaultSelective(true)
	bot.DefaultCleanInitialUsername(true)
	bot.DefaultAllowWithoutSlashInMention(true)

	go func() {
		bot.SimpleStart()
	}()
}

func TelegramBotBroadcastMessageMessage(message string) {
	channel <- message
}
