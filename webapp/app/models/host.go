package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"monitormap/webapp/app/modules"
	"net/http"
	"os/exec"
	"runtime"
	"strings"
	"time"

	db "github.com/eaciit/dbox"
	"github.com/eaciit/orm"
	tk "github.com/eaciit/toolkit"
	"gopkg.in/mgo.v2/bson"
)

type HostModel struct {
	orm.ModelBase `bson:"-",json:"-"`
	Id            string `bson:"_id",json:"_id"`
	IP            string `bson:"IP",json:"IP"`
	Name          string `bson:"Name",json:"Name"`
	Description   string `bson:"Description",json:"Description"`
	Latitude      string `bson:"Latitude",json:"Latitude"`
	Longitude     string `bson:"Longitude",json:"Longitude"`
	Address       string `bson:"Address",json:"Address"`
	PostCode      int    `bson:"PostCode",json:"PostCode"`
	City          string `bson:"City",json:"City"`
	MRTG_IMG_URL  string `bson:"MRTG_IMG_URL",json:"MRTG_IMG_URL"`
	Bandwidth     string `bson:"Bandwidth",json:"Bandwidth"`
	SID           string `bson:"SID",json:"SID"`

	PingInfo PingModel `bson:"-",json:"-"`
}

func NewHostModel() *HostModel {
	m := new(HostModel)
	return m
}

func (e *HostModel) RecordID() interface{} {
	return e.Id
}

func (m *HostModel) TableName() string {
	return "host"
}

func (m *HostModel) GetName() string {
	if strings.HasPrefix(strings.ToLower(m.Name), "server") {
		return m.Name
	} else {
		return fmt.Sprintf("Server %s", m.Name)
	}
}

func (m *HostModel) GetAll() ([]HostModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewHostModel().TableName()).
		Select().
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return make([]HostModel, 0), err
	}

	result := make([]HostModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return make([]HostModel, 0), err
	}

	return result, nil
}

func (m *HostModel) GetAllWithPing(isIncludeHistories bool, status string) ([]HostModel, tk.M, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewHostModel().TableName()).
		Select().Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return make([]HostModel, 0), tk.M{}, err
	}

	result := make([]HostModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return make([]HostModel, 0), tk.M{}, err
	}

	// ================= ping data
	resultPing := make([]PingModel, 0)
	if isIncludeHistories {
		fields := make([]string, 0)
		resultPing, err = NewPingModel().GetAll(fields, nil)
	} else {
		fields := []string{"_id", "HostID", "LatestPing", "LastUptime", "LastDowntime"}
		resultPing, err = NewPingModel().GetAll(fields, nil)
	}

	if err != nil {
		return make([]HostModel, 0), tk.M{}, err
	}

	for i, host := range result {
		result[i].PingInfo = *(NewPingModel())
		for _, ping := range resultPing {
			if ping.HostID == host.Id {
				result[i].PingInfo = ping
			}
		}
	}

	totalOnline, totalOffline, totalAll := 0, 0, len(result)
	for _, each := range result {
		if each.PingInfo.LatestPing.IsLive {
			totalOnline += 1
		} else {
			totalOffline += 1
		}
	}
	totals := tk.M{"All": totalAll, "Online": totalOnline, "Offline": totalOffline}

	resultFiltered := make([]HostModel, 0)
	switch status {
	case "online":
		for _, each := range result {
			if each.PingInfo.LatestPing.IsLive {
				resultFiltered = append(resultFiltered, each)
			}
		}
	case "offline":
		for _, each := range result {
			if !each.PingInfo.LatestPing.IsLive {
				resultFiltered = append(resultFiltered, each)
			}
		}
	default:
		resultFiltered = result
	}

	return resultFiltered, totals, nil
}

func (m *HostModel) GetById(id string) (HostModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewHostModel().TableName()).
		Where(db.Eq("_id", id)).
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return HostModel{}, err
	}

	result := make([]HostModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return HostModel{}, err
	}

	if len(result) == 0 {
		return HostModel{}, errors.New("Data tidak diketemukan")
	}

	host := result[0]
	ping, err := NewPingModel().GetByHostID(host.Id)
	if err != nil {
		return HostModel{}, err
	}
	if ping != nil {
		host.PingInfo = *ping
	} else {
		host.PingInfo = *(NewPingModel())
	}

	return host, nil
}

func (m *HostModel) Save() error {
	if m.Id == "" {
		m.Id = bson.NewObjectId().Hex()
	}

	err := modules.Conn.NewQuery().
		From(m.TableName()).
		Save().
		Exec(tk.M{"data": m})
	if err != nil {
		return err
	}

	return nil
}

func (m *HostModel) Delete() error {
	if m.Id == "" {
		return nil
	}

	err := modules.Conn.NewQuery().
		From(m.TableName()).
		Delete().
		Exec(tk.M{"data": m})
	if err != nil {
		return err
	}

	return nil
}

func (m *HostModel) ConstructFromPayload(r *http.Request) (*HostModel, error) {
	decoder := json.NewDecoder(r.Body)
	payload := NewHostModel()
	err := decoder.Decode(payload)
	if err != nil {
		return nil, err
	}

	return payload, nil
}

func (m *HostModel) Ping() string {
	defer func() {
		if r := recover(); r != nil {
			tk.Println("Recovered in f", r)
		}
	}()

	arg := ""
	if runtime.GOOS != "windows" {
		arg = "-c2"
	}

	ip := strings.Replace(strings.Replace(m.IP, "https://", "", -1), "http://", "", -1)
	res := strings.TrimSpace(runCommand(5, "ping", arg, ip))

	return res
}

func runCommand(timeout int, command string, args ...string) string {

	// instantiate new command
	cmd := exec.Command(command, args...)

	// get pipe to standard output
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "cmd.StdoutPipe() error: " + err.Error()
	}

	// start process via command
	if err := cmd.Start(); err != nil {
		return "cmd.Start() error: " + err.Error()
	}

	// setup a buffer to capture standard output
	var buf bytes.Buffer

	// create a channel to capture any errors from wait
	done := make(chan error)
	go func() {
		if _, err := buf.ReadFrom(stdout); err != nil {
			panic("buf.Read(stdout) error: " + err.Error())
		}
		done <- cmd.Wait()
	}()

	// block on select, and switch based on actions received
	select {
	case <-time.After(time.Duration(timeout) * time.Second):
		if err := cmd.Process.Kill(); err != nil {
			return "failed to kill: " + err.Error()
		}
		return "timeout reached, process killed"
	case err := <-done:
		if err != nil {
			close(done)
			return "process done, with error: " + err.Error()
		}
		return "process completed: " + buf.String()
	}
	return ""
}
