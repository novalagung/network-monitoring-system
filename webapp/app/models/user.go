package models

import (
	"encoding/json"
	"errors"
	"monitormap/webapp/app/modules"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"

	db "github.com/eaciit/dbox"
	"github.com/eaciit/orm"
	tk "github.com/eaciit/toolkit"
)

const (
	SessionKeyEmail string = "key user"
	SessionKeyRole  string = "key role"
	SessionKeyName  string = "key name"
)

type UserModel struct {
	orm.ModelBase `bson:"-",json:"-"`
	Id            string `bson:"_id",json:"_id"`
	Email         string `bson:"Email",json:"Email"`
	Password      string `bson:"Password",json:"Password"`
	Name          string `bson:"Name",json:"Name"`
	Role          string `bson:"Role",json:"Role"`
	IsNew         bool   `bson:"-",json:"-"`
	// Roles         []string `bson:"Roles",json:"Roles"`
}

func NewUserModel() *UserModel {
	m := new(UserModel)
	// m.Roles = make([]string, 0)
	return m
}

func (e *UserModel) RecordID() interface{} {
	return e.Id
}

func (m *UserModel) TableName() string {
	return "user"
}

func (m *UserModel) ConstructFromPayload(r *http.Request) (*UserModel, error) {
	decoder := json.NewDecoder(r.Body)
	payload := NewUserModel()
	err := decoder.Decode(payload)
	if err != nil {
		return nil, err
	}

	return payload, nil
}

func (m *UserModel) Login(c *gin.Context) (bool, error) {
	obj, err := m.GetByEmail(m.Email)
	if err != nil {
		return false, err
	}

	if obj.Email == "" {
		return false, errors.New("User tidak terdaftar")
	}

	if obj.Password != m.HashPassword() {
		return false, errors.New("Password salah")
	}

	sess := sessions.Default(c)
	sess.Set(SessionKeyEmail, obj.Email)
	sess.Set(SessionKeyRole, obj.Role)
	sess.Set(SessionKeyName, obj.Name)
	err = sess.Save()
	if err != nil {
		return false, err
	}

	return true, nil
}

func (m *UserModel) Logout(c *gin.Context) {
	sess := sessions.Default(c)
	sess.Clear()
	sess.Save()
}

func (m *UserModel) GetAll() ([]UserModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewUserModel().TableName()).
		Select().
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return make([]UserModel, 0), err
	}

	result := make([]UserModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return make([]UserModel, 0), err
	}

	return result, nil
}

func (m *UserModel) GetById(id string) (UserModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewUserModel().TableName()).
		Where(db.Eq("_id", id)).
		Select().
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return UserModel{}, err
	}

	result := make([]UserModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return UserModel{}, err
	}

	if len(result) == 0 {
		return UserModel{}, errors.New("User tidak terdaftar")
	}

	return result[0], nil
}

func (m *UserModel) GetByEmail(email string) (UserModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewUserModel().TableName()).
		Where(db.Eq("Email", email)).
		Select().
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return UserModel{}, err
	}

	result := make([]UserModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return UserModel{}, err
	}

	if len(result) == 0 {
		return UserModel{}, errors.New("User tidak terdaftar")
	}

	return result[0], nil
}

func (m *UserModel) Save() error {
	if m.Id == "" {
		m.Id = bson.NewObjectId().Hex()
	}

	shouldHashPassword := false
	if m.IsNew {
		shouldHashPassword = true
	} else {
		if m.Password == "" {
			oldData, err := NewUserModel().GetById(m.Id)
			if err != nil {
				return err
			}

			m.Password = oldData.Password
		} else {
			shouldHashPassword = true
		}
	}

	if shouldHashPassword {
		m.Password = m.HashPassword()
	}

	tk.Println("data", tk.JsonString(m))

	err := modules.Conn.NewQuery().
		From(m.TableName()).
		Save().
		Exec(tk.M{"data": m})
	if err != nil {
		return err
	}

	return nil
}

func (m *UserModel) HashPassword() string {
	return tk.MD5String(tk.Sprintf("%s%s%s", m.Password, modules.PasswordSalt))
}

func (m *UserModel) Delete() error {
	err := modules.Conn.NewQuery().
		From(m.TableName()).
		Delete().
		Exec(tk.M{"data": m})
	if err != nil {
		return err
	}

	return nil
}
