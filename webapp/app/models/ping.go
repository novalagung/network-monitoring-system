package models

import (
	"fmt"
	db "github.com/eaciit/dbox"
	"github.com/eaciit/orm"
	tk "github.com/eaciit/toolkit"
	"github.com/korovkin/limiter"
	"gopkg.in/mgo.v2/bson"
	"monitormap/webapp/app/modules"
	"strings"
	"sync"
	"time"
)

type PingModel struct {
	orm.ModelBase `bson:"-",json:"-"`
	Id            string             `bson:"_id",json:"_id"`
	HostID        string             `bson:"HostID",json:"HostID"`
	LatestPing    PingHistoryModel   `bson:"LatestPing",json:"LatestPing"`
	LastUptime    time.Time          `bson:"LastUptime",json:"LastUptime"`
	LastDowntime  time.Time          `bson:"LastDowntime",json:"LastDowntime"`
	Uptime        float64            `bson:"-",json:"-"`
	Histories     []PingHistoryModel `bson:"-",json:"-"`
}

func NewPingModel() *PingModel {
	m := new(PingModel)
	m.Histories = make([]PingHistoryModel, 0)
	return m
}

func (e *PingModel) RecordID() interface{} {
	return e.Id
}

func (m *PingModel) TableName() string {
	return "ping"
}

func (m *PingModel) GetAll(fields []string, filter *db.Filter) ([]PingModel, error) {
	query := modules.Conn.NewQuery().
		From(NewPingModel().TableName())

	if len(fields) > 0 {
		query = query.Select(fields...)
	}

	if filter != nil {
		query = query.Where(filter)
	}

	csr, err := query.Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return make([]PingModel, 0), err
	}

	result := make([]PingModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return make([]PingModel, 0), err
	}

	hostIDs := make([]interface{}, 0)

	for i := range result {
		result[i].Uptime = result[i].CalculateUptime()
		hostIDs = append(hostIDs, result[i].HostID)
	}

	if tk.HasMember(fields, "Histories") {
		histories, err := NewPingHistoryModel().GetByHostIDs(hostIDs)
		if err != nil {
			return make([]PingModel, 0), err
		}

		for i := range result {
			result[i].Histories = make([]PingHistoryModel, 0)

			for _, history := range histories {
				if result[i].HostID == history.HostID {
					result[i].Histories = append(result[i].Histories, history)
				}
			}
		}
	}

	return result, nil
}

func (m *PingModel) GetByHostID(id string) (*PingModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewPingModel().TableName()).
		Where(db.Eq("HostID", id)).
		Select().
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return nil, err
	}

	result := make([]PingModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, err
	}

	ping := &(result[0])
	return ping, nil
}

func (m *PingModel) Save() error {
	if m.Id == "" {
		m.Id = bson.NewObjectId().Hex()
	}

	err := modules.Conn.NewQuery().
		From(m.TableName()).
		Save().
		Exec(tk.M{"data": m})
	if err != nil {
		return err
	}

	return nil
}

func (m *PingModel) SavePingResult(hostID string, isLive bool, message string) error {
	p, _ := NewPingModel().GetByHostID(hostID)
	if p == nil {
		p = NewPingModel()
	}

	p.HostID = hostID
	p.LatestPing = PingHistoryModel{
		At:     time.Now(),
		IsLive: isLive,
		Note:   message,
	}

	histories, _, err := NewPingHistoryModel().GetByHostIdAndPaging(p.HostID, 1, 0, "", "")
	if err != nil {
		return err
	}

	if len(histories) > 0 {
		if isLive {
			if !p.LatestPing.IsLive || p.LastUptime.Year() == 1 {
				p.LastUptime = time.Now()
			}
		} else {
			if p.LatestPing.IsLive || p.LastDowntime.Year() == 1 {
				p.LastDowntime = time.Now()
			}
		}
	} else {
		if isLive {
			p.LastUptime = time.Now()
		} else {
			p.LastDowntime = time.Now()
		}
	}

	err = NewPingHistoryModel().SaveHistories([]PingHistoryModel{p.LatestPing}, p.HostID)
	if err != nil {
		return err
	}

	err = p.Save()
	if err != nil {
		return err
	}

	return nil
}

func (m *PingModel) CalculateUptime() float64 {
	if m.LatestPing.IsLive && !m.LastUptime.IsZero() {
		duration := time.Since(m.LastUptime)
		return duration.Seconds()
	}

	return float64(0)
}

func InitPinger() {
	tk.Println("=====> Start pinger")

	go func() {
		mtx := new(sync.Mutex)

		counter := 0
		messages := make([]string, 0)

		// var previousState = make(map[string]bool)

		for {
			tk.Println("========================================= START PING")

			limit := limiter.NewConcurrencyLimiter(10)
			result, err := NewHostModel().GetAll()
			if err != nil {
				return
			}

			for _, host := range result {

				limit.Execute(func() {
					defer func() {
						if r := recover(); r != nil {
							fmt.Println(r)
							fmt.Println("Keep calm, the panic won't harm your application.")
						}
					}()

					isLive := false
					message := host.Ping()
					messageLower := strings.ToLower(message)
					if strings.Contains(messageLower, "received") || strings.Contains(messageLower, "time=") {
						isLive = true
					}

					mtx.Lock()
					err = NewPingModel().SavePingResult(host.Id, isLive, message)
					if err != nil {
						tk.Println("=================", err.Error())
					}
					mtx.Unlock()

					if !isLive {
						messages = append(messages, fmt.Sprintf("%s (IP: %s) DOWN pada %s", host.Name, host.IP, time.Now().Format("2006-01-02 15:04:05")))
					}

					// previousState[host.IP] = isLive
					tk.Println("    => Ping Host:", host.IP, "      ", "isLive:", isLive, "message:", message)
				})
			}

			limit.Wait()
			counter += 1

			if counter%modules.Config.GetInt("telegramBroadcastEveryNping") == 0 {
				if len(messages) > 0 {
					for _, zz := range messages {
						go TelegramBotBroadcastMessageMessage(zz)
					}
				}
			}

			pingDelay := time.Duration(modules.Config.GetInt("pingDelay")) * time.Second
			tk.Println("=====>", pingDelay, "seconds delay before next pings")
			time.Sleep(pingDelay)
		}
	}()
}
