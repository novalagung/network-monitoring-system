package models

import (
	"monitormap/webapp/app/modules"
	"time"

	db "github.com/eaciit/dbox"
	"github.com/eaciit/orm"
	tk "github.com/eaciit/toolkit"
	"gopkg.in/mgo.v2/bson"
)

type PingHistoryModel struct {
	orm.ModelBase `bson:"-",json:"-"`
	Id            string    `bson:"_id",json:"_id"`
	HostID        string    `bson:"HostID",json:"HostID"`
	At            time.Time `bson:"At",json:"At"`
	IsLive        bool      `bson:"IsLive",json:"IsLive"`
	Note          string    `bson:"Note",json:"Note"`
}

func NewPingHistoryModel() *PingHistoryModel {
	m := new(PingHistoryModel)
	return m
}

func (e *PingHistoryModel) RecordID() interface{} {
	return e.Id
}

func (m *PingHistoryModel) TableName() string {
	return "ping_history"
}

func (m *PingHistoryModel) GetByHostIdAndPaging(id string, take, skip int, startDate, finishDate string) ([]PingHistoryModel, int, error) {
	pipesDate := make([]tk.M, 0)
	jsISOString := `2006-01-02T15:04:05.999Z07:00`

	if startDate != "" {
		start, err := time.Parse(jsISOString, startDate)
		if err == nil {
			pipesDate = append(pipesDate, tk.M{"At": tk.M{"$gte": start}})
		} else {
			tk.Println(err.Error())
		}
	}
	if finishDate != "" {
		finish, err := time.Parse(jsISOString, finishDate)
		finish = finish.AddDate(0, 0, 1)
		if err == nil {
			pipesDate = append(pipesDate, tk.M{"At": tk.M{"$lt": finish}})
		} else {
			tk.Println(err.Error())
		}
	}

	match := tk.M{"HostID": id}
	if len(pipesDate) > 0 {
		match.Set("$and", pipesDate)
	}

	// ========= get rows

	pipes := []tk.M{
		tk.M{"$match": match},
		tk.M{"$group": tk.M{
			"_id":          "$_id",
			"currentEvent": tk.M{"$last": "$event.status"},
			"events":       tk.M{"$push": "$$ROOT"},
		}},
		tk.M{"$sort": tk.M{"events.At": -1}},
		tk.M{"$skip": skip},
		tk.M{"$limit": take},
	}

	tk.Println("====== pipes", tk.JsonString(pipes))

	rows := make([]PingHistoryModel, 0)

	csr, err := modules.Conn.NewQuery().
		From(NewPingHistoryModel().TableName()).
		Command("pipe", pipes).
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return rows, 0, err
	}

	result := make([]tk.M, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return rows, 0, err
	}

	if len(result) == 0 {
		return rows, 0, nil
	}

	for _, each := range result {
		eachModel := PingHistoryModel{}
		eachRaw := each["events"].([]interface{})[0]
		err = tk.Unjson(tk.Jsonify(eachRaw), &eachModel)
		if err != nil {
			return rows, 0, err
		}

		rows = append(rows, eachModel)
	}

	// ========= total

	total := 0

	pipes = []tk.M{
		tk.M{"$match": match},
		tk.M{"$group": tk.M{"_id": "$_id"}},
		tk.M{"$group": tk.M{
			"_id":   1,
			"total": tk.M{"$sum": 1},
		}},
	}

	tk.Println("====== pipes", tk.JsonString(pipes))

	csr, err = modules.Conn.NewQuery().
		From(NewPingHistoryModel().TableName()).
		Command("pipe", pipes).
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return rows, 0, err
	}

	result = make([]tk.M, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return rows, 0, err
	}

	if len(result) > 0 {
		total = result[0].GetInt("total")
	}

	return rows, total, nil
}

func (m *PingHistoryModel) GetByHostIDs(ids []interface{}) ([]PingHistoryModel, error) {
	csr, err := modules.Conn.NewQuery().
		From(NewPingHistoryModel().TableName()).
		Where(db.In("HostID", ids...)).
		Select().
		Cursor(nil)
	if csr != nil {
		defer csr.Close()
	}
	if err != nil {
		return nil, err
	}

	result := make([]PingHistoryModel, 0)
	err = csr.Fetch(&result, 0, false)
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, err
	}

	return result, nil
}

func (m *PingHistoryModel) SaveHistories(data []PingHistoryModel, hostID string) error {
	query := modules.Conn.NewQuery().
		From(m.TableName()).
		SetConfig("multiexec", true).
		Save()
	defer query.Close()

	for _, each := range data {
		each.Id = bson.NewObjectId().Hex()
		each.HostID = hostID
		err := query.Exec(tk.M{"data": each})
		if err != nil {
			return err
		}
	}

	return nil
}
