package models

type Role struct {
	Value string
	Name  string
}

var (
	OptionRoleReadOnly   = Role{Value: "read only", Name: "Read-only User"}
	OptionRoleAdmin      = Role{Value: "admin", Name: "Administrator"}
	OptionRoleSuperAdmin = Role{Value: "super admin", Name: "Super Administrator"}
	OptionRoles          = []Role{OptionRoleReadOnly, OptionRoleAdmin, OptionRoleSuperAdmin}
)
