package modules

import (
	"io/ioutil"
	"log"
	"os"
	"strings"

	tk "github.com/eaciit/toolkit"
	"github.com/gin-gonic/gin"
)

var (
	BasePath      = func(dir string, err error) string { return dir }(os.Getwd())
	Config        = make(tk.M)
	Router        = gin.Default()
	SessionKey    = "sesyon keyz"
	PasswordSalt  = "salt isy noth swit"
	IsJustStarted = true
)

func InitConfig() {
	log.Println("Reading configuration file")

	b, err := ioutil.ReadFile("app.config.json")
	if err != nil {
		log.Println(err.Error())
		os.Exit(0)
	}

	err = tk.Unjson(b, &Config)
	if err != nil {
		log.Println(err.Error())
		os.Exit(0)
	}
}

func GetDefaultMapCoordinate() tk.M {
	coordinate := Config.GetString("mapCoordinate")
	if coordinate == "" {
		log.Println("Default coordinate in app.config.json cannot be empty")
		os.Exit(0)
	}

	return tk.M{
		"Latitude":  tk.ToFloat64(strings.TrimSpace(strings.Split(coordinate, ",")[0]), 7, tk.RoundingAuto),
		"Longitude": tk.ToFloat64(strings.TrimSpace(strings.Split(coordinate, ",")[1]), 7, tk.RoundingAuto),
	}
}
