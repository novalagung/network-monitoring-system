package modules

import (
	db "github.com/eaciit/dbox"
	_ "github.com/eaciit/dbox/dbc/mongo"
	tk "github.com/eaciit/toolkit"
	"log"
	"os"
)

var (
	Conn db.IConnection
)

func InitDatabase() {
	log.Println("Connecting to mongo server")

	connInfo := &db.ConnectionInfo{
		Host:     Config.GetString("dbHost"),
		Database: Config.GetString("dbName"),
		UserName: Config.GetString("dbUsername"),
		Password: Config.GetString("dbPassword"),
		Settings: tk.M{}.Set("timeout", Config.GetFloat64("dbTimeout")),
	}

	var err error
	Conn, err = db.NewConnection("mongo", connInfo)
	if err != nil {
		log.Println(err.Error())
		os.Exit(0)
	}

	err = Conn.Connect()
	if err != nil {
		log.Println(err.Error())
		os.Exit(0)
	}

}
