package controllers

import (
	"errors"
	"monitormap/webapp/app/models"
	"net/http"

	tk "github.com/eaciit/toolkit"
	"github.com/gin-gonic/gin"
)

type UserController struct {
}

func (h *UserController) PageIndex() func(*gin.Context) {
	return RouteWrap("page-master-user.html", func(c *gin.Context) bool {
    	if !IsLoggedIn(c) {
			c.Redirect(http.StatusTemporaryRedirect, "/")
			return false
    	}

		if IsRole(c, models.OptionRoleReadOnly) {
			c.Redirect(http.StatusTemporaryRedirect, "/page/map")
			return false
		}

		return true
	})
}

func (h *UserController) Read(c *gin.Context) {
	if !IsLoggedIn(c) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	resultHost, err := models.NewUserModel().GetAll()
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(resultHost))
}

func (h *UserController) Save(c *gin.Context) {
	if !IsRole(c, models.OptionRoleSuperAdmin) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	payload, err := models.NewUserModel().ConstructFromPayload(c.Request)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	err = payload.Save()
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(true))
}

func (h *UserController) Delete(c *gin.Context) {
	if !IsRole(c, models.OptionRoleSuperAdmin) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	payload, err := models.NewUserModel().ConstructFromPayload(c.Request)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	err = payload.Delete()
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(true))
}
