package controllers

import (
	"github.com/gin-gonic/gin"
    "net/http"
)

type MapController struct {
}

func (h *MapController) PageMap() func(*gin.Context) {
	return RouteWrap("page-map-index.html", func(c *gin.Context) bool {
		return IsLoggedIn(c)
	})
}

func (h *MapController) PageMapDetail() func(*gin.Context) {
	return RouteWrap("page-map-detail.html", func(c *gin.Context) bool {
    	if !IsLoggedIn(c) {
			c.Redirect(http.StatusTemporaryRedirect, "/")
			return false
    	}

		return true
	})
}
