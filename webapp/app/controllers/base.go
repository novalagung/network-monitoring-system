package controllers

import (
	"monitormap/webapp/app/models"
	"monitormap/webapp/app/modules"
	"net/http"
	"path/filepath"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func RouteWrap(viewName string, preHook func(*gin.Context) bool) func(*gin.Context) {
	if modules.Config.GetString("environment") == "development" {
		ReloadViews()
	}

	return func(c *gin.Context) {
		sess := sessions.Default(c)

		if modules.IsJustStarted {
			modules.IsJustStarted = false
			sess.Clear()
			sess.Save()
		}

		if !preHook(c) {
			c.Redirect(http.StatusTemporaryRedirect, "/page/login")
			return
		}

		loggedInName := ""
		sessionNameRaw := sess.Get(models.SessionKeyName)
		if sessionNameRaw != nil {
			loggedInName = sessionNameRaw.(string)
		}

		loggedInRole := ""
		sessionRoleRaw := sess.Get(models.SessionKeyRole)
		if sessionRoleRaw != nil {
			loggedInRole = sessionRoleRaw.(string)
		}

		params := make(map[string]string)
		for _, each := range c.Params {
			params[each.Key] = each.Value
		}

		viewData := map[string]interface{}{
			"HostModel":            models.NewHostModel(),
			"PingModel":            models.NewPingModel(),
			"UserModel":            models.NewUserModel(),
			"MapCenterCoordinate":  modules.GetDefaultMapCoordinate(),
			"MapZoom":              modules.Config.GetInt("mapZoom"),
			"MapRefreshDelay":      modules.Config.GetInt("mapRefreshDelay"),
			"LoggedInName":         loggedInName,
			"LoggedInRole":         loggedInRole,
			"OptionRoles":          models.OptionRoles,
			"OptionRoleAdmin":      models.OptionRoleAdmin,
			"OptionRoleSuperAdmin": models.OptionRoleSuperAdmin,
			"OptionRoleReadOnly":   models.OptionRoleReadOnly,
			"Params":               params,
		}

		c.HTML(http.StatusOK, viewName, viewData)
	}
}

func ReloadViews() {
	modules.Router.LoadHTMLGlob(filepath.Join(modules.BasePath, "app/views/*"))
}

func IsLoggedIn(c *gin.Context) bool {
	sess := sessions.Default(c)
	sessionEmailRaw := sess.Get(models.SessionKeyEmail)
	if sessionEmailRaw == nil {
		return false
	}

	return sessionEmailRaw.(string) != ""
}

func IsRole(c *gin.Context, role models.Role) bool {
	if !IsLoggedIn(c) {
		return false
	}

	sess := sessions.Default(c)
	sessionRoleRaw := sess.Get(models.SessionKeyRole)
	if sessionRoleRaw == nil {
		return false
	}

	return sessionRoleRaw.(string) == role.Value
}
