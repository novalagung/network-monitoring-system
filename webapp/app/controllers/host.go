package controllers

import (
	"errors"
	"monitormap/webapp/app/models"
	"net/http"

	tk "github.com/eaciit/toolkit"
	"github.com/gin-gonic/gin"
)

type HostController struct {
}

func (h *HostController) PageIndex() func(*gin.Context) {
	return RouteWrap("page-master-host.html", func(c *gin.Context) bool {
		if !IsLoggedIn(c) {
			c.Redirect(http.StatusTemporaryRedirect, "/")
			return false
		}

		if IsRole(c, models.OptionRoleReadOnly) {
			c.Redirect(http.StatusTemporaryRedirect, "/page/map")
			return false
		}

		return true
	})
}

func (h *HostController) ReadWithoutHistories(c *gin.Context) {
	if !IsLoggedIn(c) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	status := c.Request.URL.Query().Get("status")
	resultHost, totals, err := models.NewHostModel().GetAllWithPing(false, status)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(tk.M{
		"Rows":   resultHost,
		"Totals": totals,
	}))
}

func (h *HostController) ReadDetail(c *gin.Context) {
	if !IsLoggedIn(c) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	hostID := c.Param("id")
	take := tk.ToInt(c.Request.URL.Query().Get("take"), tk.RoundingAuto)
	skip := tk.ToInt(c.Request.URL.Query().Get("skip"), tk.RoundingAuto)
	startDate := c.Request.URL.Query().Get("startdate")
	finishDate := c.Request.URL.Query().Get("finishdate")

	info, err := models.NewHostModel().GetById(hostID)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	rows, total, err := models.NewPingHistoryModel().GetByHostIdAndPaging(hostID, take, skip, startDate, finishDate)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(tk.M{
		"Rows":  rows,
		"Total": total,
		"Info":  info,
	}))
}

func (h *HostController) Save(c *gin.Context) {
	if IsRole(c, models.OptionRoleReadOnly) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	payload, err := models.NewHostModel().ConstructFromPayload(c.Request)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	err = payload.Save()
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(true))
}

func (h *HostController) Delete(c *gin.Context) {
	if IsRole(c, models.OptionRoleReadOnly) {
		c.JSON(http.StatusOK, tk.NewResult().SetError(errors.New("Anda tidak mempunyai akses terhadap operasi ini")))
		return
	}

	payload, err := models.NewHostModel().ConstructFromPayload(c.Request)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	err = payload.Delete()
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(true))
}
