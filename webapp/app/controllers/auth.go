package controllers

import (
	"monitormap/webapp/app/models"
	"net/http"

	tk "github.com/eaciit/toolkit"
	"github.com/gin-gonic/gin"
)

type AuthController struct {
}

func (h *AuthController) PageIndex() func(*gin.Context) {
	return RouteWrap("page-auth-login.html", func(c *gin.Context) bool {
		if IsLoggedIn(c) {
			c.Redirect(http.StatusTemporaryRedirect, "/page/map")
		}

		return true
	})
}

func (h *AuthController) DoLogin(c *gin.Context) {
	payload, err := models.NewUserModel().ConstructFromPayload(c.Request)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	result, err := payload.Login(c)
	if err != nil {
		c.JSON(http.StatusOK, tk.NewResult().SetError(err))
		return
	}

	c.JSON(http.StatusOK, tk.NewResult().SetData(result))
}

func (h *AuthController) DoLogout(c *gin.Context) {
	models.NewUserModel().Logout(c)
	c.Redirect(http.StatusTemporaryRedirect, "/page/login")
}
