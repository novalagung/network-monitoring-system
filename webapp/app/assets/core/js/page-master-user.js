var mu = {}
viewModel.mu = mu

mu.userModelTemplate = {}
mu.selectedData = undefined
mu.crudMode = ko.observable('')
mu.data = ko.observable([])
mu.confirmPassword = ko.observable('')
mu.optionRoles = ko.observableArray([])

mu.renderGrid = function () {
    if ($('#grid').data('kendoGrid') !== undefined) {
        $('#grid').data('kendoGrid').setDataSource(new kendo.data.DataSource({
            data: mu.data()
        }))
        return
    }

    var config = {
        dataSource: {
            data: mu.data(),
            pageSize: 10
        },
        pageable: true,
        filterable: true,
        sortable: true,
        columns: [{
            field: 'Email'
        }, {
            field: 'Name',
            title: 'Nama User'
        }, {
            field: 'Role',
            template: function (d) {
                let roleRow = mu.optionRoles().find(function (g) {
                    return g.Value == d.Role
                })
                if (typeof roleRow == 'undefined') {
                    return ''
                }

                return roleRow.Name
            }
            // field: 'Roles',
            // template: function (d) {
            //     return d.Roles.map(function (g) {
            //         return '- ' + g
            //     }).join('\n')
            // }
        }, {
            headerTemplate: function () {
                return '<center>'
                    + '<button class="btn btn-sm btn-success" onclick="mu.create()">'
                    + '<i class="fa fa-plus"></i>&nbsp; Tambah data'
                    + '</button>'
                    + '</center>'
            },
            width: 150,
            hidden: l.isAdmin(),
            template: function (d) {
                return '<center>'
                    + '<button class="btn btn-sm btn-primary" onclick="mu.edit(\'' + d.Email + '\')">Edit</button> &nbsp;'
                    + '<button class="btn btn-sm btn-danger" onclick="mu.delete(\'' + d.Email + '\')">Hapus</button>'
                    + '</center>'
            }
        }]
    }

    $('#grid').kendoGrid(config)
}

mu.getPayload = function () {
    var payload = ko.mapping.toJS(mu.selectedData)
    return JSON.stringify(payload)
}

mu.get = function () {
    $.ajax({
        type: 'GET',
        url: '/master/user',
        dataType: "json",
        contentType: "application/json",
    }).success(function (res) {
        if (res.Status !== "OK") {
            swal('Error!', res.Message, 'error')
            return
        }

        mu.data(_.orderBy(res.Data, 'Name'))
        mu.renderGrid()
    }).error(function (err) {
       swal('Error!', err.responseText, 'error')
    })
}

mu.create = function () {
    mu.crudMode('new')
    $('#modal-edit').modal('show')

    setTimeout(function () {
        ko.mapping.fromJS(mu.userModelTemplate, mu.selectedData)
        mu.selectedData.IsNew(true)
        mu.confirmPassword('')
        $('input[type=password]').prop('required', true)
        $('input[name=Email]').prop('disabled', false)
        // $('#modal-edit form').validator()
    }, 301)
}

mu.edit = function (id) {
    var selectedRow = mu.data().find(function (d) {
        return d.Email == id
    })
    if (selectedRow === undefined) {
        return
    }

    mu.crudMode('edit')
    $('#modal-edit').modal('show')

    setTimeout(function () {
        ko.mapping.fromJS(selectedRow, mu.selectedData)
        mu.selectedData.Password('')
        mu.selectedData.IsNew(false)
        mu.confirmPassword('')
        $('input[type=password]').prop('required', false)
        $('input[name=Email]').prop('disabled', true)
        // $('#modal-edit form').validator()
    }, 301)
}

mu.delete = function (id) {
    var selectedRow = mu.data().find(function (d) {
        return d.Email == id
    })
    if (selectedRow === undefined) {
        return
    }
    payload = ko.mapping.toJSON(selectedRow)

    swal({
        title: 'Apakah anda yakin?',
        text: "Data yang sudah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#F03434',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Tidak'
    }, function () {
        $.ajax({
            type: 'DELETE',
            url: '/master/user',
            dataType: "json",
            contentType: "application/json",
            data: payload
        }).success(function (res) {
            if (res.Status !== "OK") {
                swal('Error!', res.Message, 'error')
                return
            }

            setTimeout(function () {
                swal('Sukses!', 'Data berhasil dihapus', 'success')
                mu.get()
            }, 300)
        }).error(function (err) {
           swal('Error!', err.responseText, 'error')
        })
    })
}

mu.save = function () {
    if ($.trim(mu.selectedData.Email()) == '') {
        swal('Error!', 'Email tidak boleh kosong', 'error')
        return
    }

    var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegex.test(mu.selectedData.Email())) {
        swal('Error!', 'Email tidak valid', 'error')
        return
    }

    if ($.trim(mu.selectedData.Name()) == '') {
        swal('Error!', 'Nama tidak boleh kosong', 'error')
        return
    }

    if (mu.crudMode() == 'new') {
        var confirmPassword = mu.confirmPassword()

        if ($.trim(mu.selectedData.Password()) == '') {
            swal('Error!', 'Password tidak boleh kosong', 'error')
            return
        }
        if ($.trim(confirmPassword) == '') {
            swal('Error!', 'Konfirmasi Password tidak boleh kosong', 'error')
            return
        }
        if (mu.selectedData.Password() != confirmPassword) {
            swal('Error!', 'Password tidak sama', 'error')
            return
        }
    }

    if ($.trim(mu.selectedData.Role()) == '') {
        swal('Error!', 'Role tidak boleh kosong', 'error')
        return
    }

    var payload = mu.getPayload()
    $.ajax({
        type: 'POST',
        url: '/master/user',
        dataType: "json",
        contentType: "application/json",
        data: payload
    }).success(function (res) {
        if (res.Status !== "OK") {
            swal('Error!', res.Message, 'error')
            return
        }

        swal('Sukses!', 'Data berhasil disimpan', 'success')
        $('#modal-edit').modal('hide')
        mu.get()
    }).error(function (err) {
        swal('Error!', err.responseText, 'error')
    })
}

mu.init = function () {
    mu.selectedData = ko.mapping.fromJS(mu.userModelTemplate)
    mu.get()
}
