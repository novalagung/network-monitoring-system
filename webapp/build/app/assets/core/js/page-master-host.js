var mh = {}
viewModel.mh = mh

mh.hostModelTemplate = {}
mh.selectedData = undefined
mh.crudMode = ko.observable('')
mh.data = ko.observable([])

mh.renderGrid = function () {
    if ($('#grid').data('kendoGrid') !== undefined) {
        $('#grid').data('kendoGrid').setDataSource(new kendo.data.DataSource({
            data: mh.data()
        }))
        return
    }

    var config = {
        dataSource: {
            data: mh.data(),
            pageSize: 10
        },
        pageable: true,
        filterable: true,
        sortable: true,
        columns: [{
            field: 'IP'
        }, {
            field: 'Name',
            title: 'Nama Server'
        }, {
            headerTemplate: '<center>Koordinat</center>',
            columns: [{
                field: 'Latitude'
            }, {
                field: 'Longitude'
            }]
        }, {
            headerTemplate: '<center>Alamat</center>',
            columns: [{
                field: 'Address',
                title: 'Nama Jalan'
            }, {
                field: 'City',
                title: 'Kota'
            }, {
                field: 'PostCode',
                title: 'Kode Pos'
            }]
        }, {
            headerTemplate: function () {
                return '<center>'
                    + '<button class="btn btn-sm btn-success" onclick="mh.create()">'
                    + '<i class="fa fa-plus"></i>&nbsp; Tambah data'
                    + '</button>'
                    + '</center>'
            },
            width: 150,
            template: function (d) {
                return '<center>'
                    + '<button class="btn btn-sm btn-primary" onclick="mh.edit(\'' + d.Id + '\')">Edit</button> &nbsp;'
                    + '<button class="btn btn-sm btn-danger" onclick="mh.delete(\'' + d.Id + '\')">Hapus</button>'
                    + '</center>'
            }
        }]
    }

    $('#grid').kendoGrid(config)
}

mh.getPayload = function () {
    var payload = ko.mapping.toJS(mh.selectedData)
    payload.Latitude = String(payload.Latitude)
    payload.Longitude = String(payload.Longitude)
    payload.PostCode = parseInt(payload.PostCode, 10)
    return JSON.stringify(payload)
}

mh.get = function () {
    $.ajax({
        type: 'GET',
        url: '/master/host/without-histories',
        dataType: "json",
        contentType: "application/json",
    }).success(function (res) {
        if (res.Status !== "OK") {
            swal('Error!', res.Message, 'error')
            return
        }

        mh.data(_.orderBy(res.Data.Rows, 'Name'))
        mh.renderGrid()
    }).error(function (err) {
       swal('Error!', err.responseText, 'error')
    })
}

mh.create = function () {
    mh.prepareMap()
    mh.crudMode('new')
    $('#modal-edit').modal('show')

    setTimeout(function () {
        ko.mapping.fromJS(mh.hostModelTemplate, mh.selectedData)
        // $('#modal-edit form').validator()
    }, 301)
}

mh.edit = function (id) {
    mh.prepareMap()
    var selectedRow = mh.data().find(function (d) {
        return d.Id == id
    })
    if (selectedRow === undefined) {
        return
    }

    mh.crudMode('edit')
    $('#modal-edit').modal('show')

    setTimeout(function () {
        ko.mapping.fromJS(selectedRow, mh.selectedData)

        var interval = setInterval(function () {
            if (mh.mapObject == null || mh.mapObject == undefined) {
                return
            }

            clearInterval(interval)

            var position = new google.maps.LatLng(
                parseFloat(mh.selectedData.Latitude()),
                parseFloat(mh.selectedData.Longitude())
            )
            mh.mapObject.setCenter(position)
            mh.addMarker(position)
        }, 100)
        // $('#modal-edit form').validator()
    }, 301)
}

mh.delete = function (id) {
    var selectedRow = mh.data().find(function (d) {
        return d.Id == id
    })
    if (selectedRow === undefined) {
        return
    }
    payload = ko.mapping.toJSON(selectedRow)

    swal({
        title: 'Apakah anda yakin?',
        text: "Data yang sudah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#F03434',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Tidak'
    }, function () {
        $.ajax({
            type: 'DELETE',
            url: '/master/host',
            dataType: "json",
            contentType: "application/json",
            data: payload
        }).success(function (res) {
            if (res.Status !== "OK") {
                swal('Error!', res.Message, 'error')
                return
            }

            setTimeout(function () {
                swal('Sukses!', 'Data berhasil dihapus', 'success')
                mh.get()
            }, 300)
        }).error(function (err) {
           swal('Error!', err.responseText, 'error')
        })
    })
}

mh.save = function () {
    if ($.trim(mh.selectedData.IP) == '') {
        swal('Error!', 'IP tidak boleh kosong', 'error')
        return
    }
    if ($.trim(mh.selectedData.Name) == '') {
        swal('Error!', 'Nama server tidak boleh kosong', 'error')
        return
    }
    if ($.trim(mh.selectedData.Latitude) == '') {
        swal('Error!', 'Latitude tidak boleh kosong', 'error')
        return
    }
    if ($.trim(mh.selectedData.Longitude) == '') {
        swal('Error!', 'Longitude tidak boleh kosong', 'error')
        return
    }

    var payload = mh.getPayload()
    $.ajax({
        type: 'POST',
        url: '/master/host',
        dataType: "json",
        contentType: "application/json",
        data: payload
    }).success(function (res) {
        if (res.Status !== "OK") {
            swal('Error!', res.Message, 'error')
            return
        }

        swal('Sukses!', 'Data berhasil disimpan', 'success')
        $('#modal-edit').modal('hide')
        mh.get()
    }).error(function (err) {
        swal('Error!', err.responseText, 'error')
    })
}

mh.mapCenterCoordinate = {}
mh.mapZoom = 15
mh.mapRefreshDelay = 15
mh.mapObject = undefined
mh.markers = ko.observableArray([])

mh.prepareMap = function () {
    if (typeof mh.mapObject !== 'undefined') {
        return
    }

    var mapOptions = {
        zoom: mh.mapZoom,
        center: new google.maps.LatLng(mh.mapCenterCoordinate.Latitude, mh.mapCenterCoordinate.Longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        // zoomControl: false,
        // zoomControlOptions: {
        //     style: google.maps.ZoomControlStyle.LARGE,
        //     position: google.maps.ControlPosition.TOP_RIGHT
        // },
        scaleControl: false,
        scaleControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: false,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        styles: [ /*insert your map styles*/ ]
    }

    setTimeout(function () {
        mh.mapObject = new google.maps.Map(document.getElementById('map'), mapOptions)
        google.maps.event.addListener(mh.mapObject, 'click', function(event) {
            mh.addMarker(event.latLng)
        });
    }, 1000)
}

mh.addMarker = function (position) {
    mh.markers().forEach(function (marker) {
        marker.setMap(null)
    })
    mh.markers([])
    var marker = new google.maps.Marker({
        position: position,
        map: mh.mapObject,
        icon: '/static/core/images/markers/Showplace.png',
    })
    mh.markers.push(marker)

    mh.selectedData.Latitude(String(position.lat()))
    mh.selectedData.Longitude(String(position.lng()))
}

mh.init = function () {
    mh.selectedData = ko.mapping.fromJS(mh.hostModelTemplate)
    mh.get()
}
