var l = {}
viewModel.l = l

l.userData = ko.mapping.fromJS({
    Email: '',
    Password: ''
})

l.registerSubmitEvent = function () {
    $('form').on('submit', function (e) {
        e.preventDefault()

        var payload = ko.mapping.toJSON(l.userData)
        if (payload.Email == '') {
            swal('Log In gagal!', 'Username tidak boleh kosong', 'error')
            return
        }
        if (payload.Password == '') {
            swal('Log In gagal!', 'Password tidak boleh kosong', 'error')
            return
        }

        var $submitButton = $(this).find('button, input')
        $submitButton.prop('disabled', true)

        $.ajax({
            type: 'POST',
            url: '/login',
            dataType: "json",
            contentType: "application/json",
            data: payload
        }).success(function (res) {
            setTimeout(function () {
                $submitButton.prop('disabled', false)

                if (res.Status !== 'OK') {
                    swal("Log In gagal!", res.Message, "error")
                    return
                }

                swal({
                    title: 'Log In sukses',
                    text: 'Halaman dashboard akan terbuka secara otomatis dalam beberapa detik',
                    type: 'success',
                    timer: 2000,
                    showConfirmButton: false
                }, function () {
                    location.href = "/page/map"
                })
            }, 500)
        }).error(function (err) {
            setTimeout(function () {
                $submitButton.prop('disabled', false)
                swal('Log In gagal!', err.responseText, 'error')
            }, 500)
        })
    })
}

$(function () {
    l.registerSubmitEvent()
})
