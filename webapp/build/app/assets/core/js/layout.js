var l = {}
viewModel.l = l

l.loggedInName = ko.observable('')
l.loggedInRole = ko.observable('')
l.optionsRoles = ko.observableArray([])
l.optionsRoleAdmin = ko.observable('')
l.optionsRoleSuperAdmin = ko.observable('')
l.optionsRolesReadOnly = ko.observable('')

l.isAdmin = ko.computed(function () {
    return l.loggedInRole() == l.optionsRoleAdmin()
}, {
    role: l.loggedInRole,
    roleTarget: l.optionsRoleAdmin,
})

l.isSuperAdmin = ko.computed(function () {
    return l.loggedInRole() == l.optionsRoleSuperAdmin()
}, {
    role: l.loggedInRole,
    roleTarget: l.optionsRoleSuperAdmin,
})

l.isReadOnly = ko.computed(function () {
    return l.loggedInRole() == l.optionsRolesReadOnly()
}, {
    role: l.loggedInRole,
    roleTarget: l.optionsRolesReadOnly,
})

l.printSwalError = function (message, callback) {
    swal({
        title: "Error!",
        text: res.Message,
        icon: "error",
    }, function () {
        setTimeout(function () {
            callback
        }, 300);
    })
}
