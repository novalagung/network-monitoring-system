var md = {}
viewModel.md = md

md.id = ko.observable('')
md.hostModelTemplate = {}
md.selectedData = undefined
md.crudMode = ko.observable('')
md.filterStart = ko.observable('')
md.filterFinish = ko.observable('')

md.renderGrid = function () {
    var config = {
        dataSource: {
            transport: {
                read: {
                    url: '/master/host/detail/' + md.id(),
                    dataType: "json",
                    type: "GET",
                },
                parameterMap: function(data, type) {
                    var start = md.filterStart() instanceof Date ? md.filterStart().toISOString() : (md.filterStart() || '')
                    var finish = md.filterFinish() instanceof Date ? md.filterFinish().toISOString() : (md.filterFinish() || '')

                    return (
                       'skip=' + data.skip +
                       '&take=' + data.take +
                       '&startdate=' + start +
                       '&finishdate=' + finish
                   );
               }
           },
            schema: {
                data: function (d) {
                    ko.mapping.fromJS(d.Data.Info, md.selectedData)
                    return d.Data.Rows
                },
                total: function (d) {
                    return d.Data.Total
                }
            },
            pageSize: 10,
            serverPaging: true
        },
        pageable: true,
        filterable: true,
        sortable: true,
        columns: [{
            title: 'Tanggal',
            template: function (d) {
                return moment(d.At).format('DD MMMM YYYY')
            }
        }, {
            title: 'Waktu',
            template: function (d) {
                return moment(d.At).format('HH:mm:ss')
            }
        }, {
            headerTemplate: 'Status',
            template: function (d) {
                return d.IsLive
                    ? '<span style="font-weight: bold; color: #00B16A;">Online</span>'
                    : '<span style="font-weight: bold; color: #F03434;">Offline</span>'
            }
        }]
    }

    $('#grid').kendoGrid(config)
}

md.refresh = function () {
    var ds = $('#grid').data('kendoGrid').dataSource;
    ds.query({
        page: 1,
        pageSize: ds.pageSize()
    }).then(function () {
        ds.read();
    });
}

md.init = function () {
    md.selectedData = ko.mapping.fromJS(md.hostModelTemplate)
    md.renderGrid()
}
