var mb = {}
viewModel.mb = mb

mb.data = ko.observableArray([])
mb.markers = ko.observableArray([])
mb.mapCenterCoordinate = {}
mb.mapZoom = 15
mb.mapRefreshDelay = 15

mb.searchKeyword = ko.observable('')
mb.mapObject = undefined

mb.filterStatus = ko.observable('all')
mb.botGetDataInterval = null

mb.totalAll = ko.observable(0)
mb.totalOffline = ko.observable(0)
mb.totalOnline = ko.observable(0)

mb.search = ko.observable('')
mb.search.subscribe(_.debounce(function () {
    console.log('search', mb.search())
    mb.removeMarkers()
    setTimeout(mb.drawMarkers, 100)
}, 100))

mb.changeFilterStatus = function (what) {
    mb.closeInfoBox()
    if (mb.filterStatus() == what) {
        return
    }
    mb.filterStatus(what)
    mb.startBotGetData()
}

mb.startBotGetData = function () {
    if (mb.botGetDataInterval != null) {
        clearInterval(mb.botGetDataInterval)
    }

    mb.get()
    mb.botGetDataInterval = setInterval(mb.get, mb.mapRefreshDelay)
}

mb.get = function () {
    var payload = {
        status: mb.filterStatus()
    }

    $.ajax({
        type: 'GET',
        url: '/master/host/without-histories',
        dataType: "json",
        contentType: "application/json",
        data: payload
    }).success(function (res) {
        if (res.Success == "OK") {
            swal('Error!', res.Message, 'error')
            return
        }

        mb.totalAll(res.Data.Totals.All)
        mb.totalOnline(res.Data.Totals.Online)
        mb.totalOffline(res.Data.Totals.Offline)

        mb.data(res.Data.Rows)
        mb.removeMarkers()
        setTimeout(mb.drawMarkers, 100)
    }).error(function (err) {
       swal('Error!', err.responseText, 'error')
    })
}

mb.init = function () {
    var mapOptions = {
        zoom: mb.mapZoom,
        center: new google.maps.LatLng(mb.mapCenterCoordinate.Latitude, mb.mapCenterCoordinate.Longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        // zoomControl: false,
        // zoomControlOptions: {
        //     style: google.maps.ZoomControlStyle.LARGE,
        //     position: google.maps.ControlPosition.TOP_RIGHT
        // },
        scaleControl: false,
        scaleControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: false,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        styles: [ /*insert your map styles*/ ]
    }

    mb.mapObject = new google.maps.Map(document.getElementById('map'), mapOptions)

    mb.startBotGetData()
}
mb.drawMarkers = function () {
    mb.data().filter(function (d) {
        var search = $.trim(mb.search()).toLowerCase()
        if (search == '') {
            return true
        }

        return d.Name.toLowerCase().indexOf(search) > -1;
    }).forEach(function(item) {
        var latitude = parseFloat(item.Latitude)
        var longitude = parseFloat(item.Longitude)
        var marker = new MarkerWithLabel({
            position: new google.maps.LatLng(item.Latitude, item.Longitude),
            map: mb.mapObject,
            draggable: true,
            raiseOnDrag: true,
            labelContent: item.Name,
            labelAnchor: new google.maps.Point(15, 65),
            labelClass: "marker-label",
            labelInBackground: false,
            icon: '/static/core/images/markers/' + (item.PingInfo.LatestPing.IsLive ? 'Showplace' : 'Shop') + '.png',
        })
        google.maps.event.addListener(marker, 'click', (function() {
            location.href = '/page/map/' + item.Id
        }))
        google.maps.event.addListener(marker, 'mouseover', (function() {
            mb.closeInfoBox()
            mb.showInfoBox(item).open(mb.mapObject, this)
            // mb.mapObject.setCenter(new google.maps.LatLng(item.Latitude, item.Longitude))
        }))
        google.maps.event.addListener(marker, 'mouseout', (function() {
            mb.closeInfoBox()
        }))

        mb.markers.push(marker)
    })

    google.maps.event.addListener(mb.mapObject, "click", function(event) {
        mb.closeInfoBox()
    });
}
mb.removeMarkers = function () {
	mb.markers().forEach(function (marker) {
		marker.setMap(null)
	})
    mb.markers([])
}
mb.formatDuration = function (duration) {
    var sec_num = parseInt(duration, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}

    if (hours <= 0) {
        if (minutes <= 0) {
            return seconds + ' detik'
        } else {
            return minutes + ' menit ' + seconds + ' detik'
        }
    } else if (minutes <= 0) {
        return seconds + ' detik'
    } else {
        return hours + ' jam ' + minutes + ' menit ' + seconds + ' detik'
    }
}
mb.showInfoBox = function(item) {
    var ipLink = item.IP.indexOf('http') > -1
        ? item.IP
        : ('http://' + item.IP)

    var lastUptime = item.PingInfo.LastUptime.indexOf('0001') == -1
        ? moment(item.PingInfo.LastUptime).format('DD MMMM YYYY HH:mm:ss')
        : '-'

    var lastDowntime = item.PingInfo.LastDowntime.indexOf('0001') == -1
        ? moment(item.PingInfo.LastDowntime).format('DD MMMM YYYY HH:mm:ss')
        : '-'

    var uptime = item.PingInfo.Uptime > 0
        ? mb.formatDuration(item.PingInfo.Uptime)
        : '-'

    return new InfoBox({
        content:
            '<div class="marker_info none" id="marker_info">' +
                '<div class="info" id="info">' +
                    '<div class="wrapper">' +
                        '<h3>' + item.Name + '</h3>' +
                        '<img src="' + item.MRTG_IMG_URL + '" />' +
                        '<table>' +
                        '<tr>' +
                        '<td>IP</td><td><a target="_blank" href="' + ipLink + '">' + item.IP + '</a></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Last Uptime</td><td>' + lastUptime + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Last Downtime</td><td>' + lastDowntime + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Uptime</td><td>' + uptime + '</td>' +
                        '</tr>' +
                        '</table>' +
                    '</div>' +
                '</div>' +
                '<span class="arrow"></span>' +
            '</div>',
        disableAutoPan: true,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(40, -210),
        closeBoxMargin: '50px 200px',
        closeBoxURL: '',
        isHidden: false,
        pane: 'floatPane',
        enableEventPropagation: true
    })
}
mb.closeInfoBox = function () {
    $('div.infoBox').remove()
}
