rm -rf build

go build -v -o app-linux

mkdir -p build/app
mv app-linux build/
cp app.config.json build/
cp -r app/assets build/app
cp -r app/views build/app
